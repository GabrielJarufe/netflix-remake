package com.gabrieljarufe.netflixremake.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.gabrieljarufe.netflixremake.model.Category;
import com.gabrieljarufe.netflixremake.model.Movie;
import com.gabrieljarufe.netflixremake.model.MovieDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MovieDetailTask extends AsyncTask<String, Void, MovieDetail> {

    private final WeakReference<Context> context;
    private ProgressDialog dialog;
    private MovieDetailLoader movieDetailLoader;

    public MovieDetailTask(Context context) {
        this.context = new WeakReference<>(context);
    }

    public void setMovieDetailLoader(MovieDetailLoader movieDetailLoader) {
        this.movieDetailLoader = movieDetailLoader;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        dialog = ProgressDialog.show(context, "Carregando", "", true);
    }

    @Override
    protected MovieDetail doInBackground(String... params) {
        String url = params[0];

        try {
            URL requestURL = new URL(url);
            HttpsURLConnection urlConnection = (HttpsURLConnection) requestURL.openConnection();
            urlConnection.setReadTimeout(2000);
            urlConnection.setConnectTimeout(2000);

            int responseCode = urlConnection.getResponseCode();
            if (responseCode > 400) {
                throw new IOException("Erro ao se comunicar com o servidor");
            }
            InputStream inputStream = urlConnection.getInputStream();

            BufferedInputStream in = new BufferedInputStream(inputStream);

            String jsonAsString = toString(in);
            //criando lista
            MovieDetail movieDetail = getMovieDetail(new JSONObject(jsonAsString));
            //fechando conexão na internet
            in.close();

            return movieDetail;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private MovieDetail getMovieDetail(JSONObject jsonObject) throws JSONException {
        int id = jsonObject.getInt("id");
        String title = jsonObject.getString("title");
        String description = jsonObject.getString("desc");
        String cast = jsonObject.getString("cast");
        String coverUrl = jsonObject.getString("cover_url");

        List<Movie> movieList = new ArrayList<>();
        JSONArray movieArrayList = jsonObject.getJSONArray("movie");
        for (int i = 0; i < movieArrayList.length(); i++){
            JSONObject movie = movieArrayList.getJSONObject(i);
            String c = movie.getString("cover_url");
            int idSimilar = movie.getInt("id");
            Movie similar = new Movie();
            similar.setId(idSimilar );
            similar.setCoverUrl(c);

            movieList.add(similar);
        }

        Movie movie = new Movie();
        movie.setId(id);
        movie.setCoverUrl(coverUrl);
        movie.setTitle(title);
        movie.setDescription(description);
        movie.setCast(cast);

        return new MovieDetail(movie, movieList);
    }

    @Override
    protected void onPostExecute(MovieDetail movieDetail) {
        super.onPostExecute(movieDetail);
        dialog.dismiss();

        if(movieDetailLoader != null)
            movieDetailLoader.onResult(movieDetail);
    }

    public interface MovieDetailLoader{
        void onResult(MovieDetail movieDetail);
    }

    private String toString(InputStream is) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0) {
            baos.write(bytes, 0, lidos);
        }

        return baos.toString();
    }

}