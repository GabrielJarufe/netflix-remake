package com.gabrieljarufe.netflixremake.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gabrieljarufe.netflixremake.R;
import com.gabrieljarufe.netflixremake.model.Category;
import com.gabrieljarufe.netflixremake.model.Movie;
import com.gabrieljarufe.netflixremake.util.CategoryTask;
import com.gabrieljarufe.netflixremake.util.ImageDownloaderTask;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CategoryTask.CategoryLoader {

    private MainAdapter mainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerViewCategory = findViewById(R.id.rv_category);

        List<Category> categories = new ArrayList<>();

        mainAdapter = new MainAdapter(categories);
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerViewCategory.setAdapter(mainAdapter);

        CategoryTask categoryTask = new CategoryTask(this);
        categoryTask.setCategoryLoader(this);
        categoryTask.execute("https://tiagoaguiar.co/api/netflix/home");
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onResult(List<Category> categories){
        mainAdapter.setCategories(categories);
        mainAdapter.notifyDataSetChanged();
    }

    private static class MovieHolder extends RecyclerView.ViewHolder{

        private final ImageView img_cover;

        public MovieHolder(@NonNull View itemView, final OnItemClicklistener onItemClicklistener) {
            super(itemView);
            img_cover = itemView.findViewById(R.id.img_cover_test);
            itemView.setOnClickListener(v -> {
                onItemClicklistener.onClick(getAdapterPosition());
            });
        }
    }

    private static class CategoryHolder extends RecyclerView.ViewHolder{

        private final TextView title;
        private final RecyclerView rvMovie;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_category_title);
            rvMovie = itemView.findViewById(R.id.rv_movie);
        }
    }

    private class MainAdapter extends RecyclerView.Adapter<CategoryHolder>{

        private List<Category> categories;

        private MainAdapter(List<Category> categories) {
            this.categories = categories;
        }

        @NonNull
        @Override
        public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new CategoryHolder(getLayoutInflater().inflate(R.layout.category_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.title.setText(category.getName());
            holder.rvMovie.setAdapter(new MovieAdapter(category.getMovies()));
            holder.rvMovie.setLayoutManager(new LinearLayoutManager(getBaseContext(), RecyclerView.HORIZONTAL,false));
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

         void setCategories(List<Category> categories) {
            this.categories.clear();
            this.categories.addAll(categories);

        }
    }


    private class MovieAdapter extends RecyclerView.Adapter<MovieHolder> implements OnItemClicklistener{

        private final List<Movie> movies;

        private MovieAdapter(List<Movie> movies) {
            this.movies = movies;
        }

        @NonNull
        @Override
        public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.movie_item, parent, false);
            return new MovieHolder(view, this);
        }

        @Override
        public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
            Movie movie = movies.get(position);
            new ImageDownloaderTask(holder.img_cover).execute(movie.getCoverUrl());
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }

        @Override
        public void onClick(int position) {
            if (movies.get(position).getId() <= 3) {
                Intent intent = new Intent(MainActivity.this, MovieActivity.class);
                intent.putExtra("id", movies.get(position).getId());
                startActivity(intent);
            }
        }
    }

    interface OnItemClicklistener{
        void onClick(int position);
    }

}