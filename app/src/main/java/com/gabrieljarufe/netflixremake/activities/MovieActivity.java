package com.gabrieljarufe.netflixremake.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gabrieljarufe.netflixremake.R;
import com.gabrieljarufe.netflixremake.model.Movie;
import com.gabrieljarufe.netflixremake.model.MovieDetail;
import com.gabrieljarufe.netflixremake.util.ImageDownloaderTask;
import com.gabrieljarufe.netflixremake.util.MovieDetailTask;

import java.util.ArrayList;
import java.util.List;

public class MovieActivity extends AppCompatActivity implements MovieDetailTask.MovieDetailLoader {

    private TextView txtTitle;
    private TextView txtDesc;
    private TextView txtCast;
    private RecyclerView rvMovieSimilar;
    private MovieAdapter movieAdapter;
    private ImageView imgMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        txtTitle = findViewById(R.id.txt_title_movie);
        txtDesc = findViewById(R.id.txt_details_movie);
        txtCast = findViewById(R.id.txt_cast_movie);
        rvMovieSimilar = findViewById(R.id.rv_similar_movies);
        imgMovie = findViewById(R.id.image_movie);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }


        LayerDrawable drawable = (LayerDrawable) ContextCompat.getDrawable(this, R.drawable.shadow_movie);

        if (drawable != null) {
            Drawable movieCover = ContextCompat.getDrawable(this, R.drawable.shadow_movie);
            drawable.setDrawableByLayerId(R.id.cover_drawable, movieCover);
            //((ImageView) findViewById(R.id.image_movie)).setImageDrawable(drawable);
        }


        txtTitle.setText("Diário de uma Paixão");
        txtDesc.setText("Na década de 40, na Carolina do Sul, o operário Noah Calhoun e a rica Allie se apaixonam desesperadamente, mas os pais da jovem não aprovam o namoro. Noah então é enviado para combater na Segunda Guerra Mundial, e parece ser o fim do romance. Enquanto isso, Allie se envolve com outro homem. No entanto, se torna claro que a paixão deles ainda não acabou quando Noah retorna para a pequena cidade anos mais tarde, próximo ao casamento de Allie.\n");
        txtCast.setText(getString(R.string.cast, "Ryan Gosling, Rachel McAdams, James Garner, Gena Rowlands, James Marsden, Joan Allen, Sam Shepard, David Thornton"));

        List<Movie> movies = new ArrayList<Movie>();
        movieAdapter = new MovieAdapter(movies);
        rvMovieSimilar.setAdapter(movieAdapter);
        rvMovieSimilar.setLayoutManager(new GridLayoutManager(MovieActivity.this, 3));

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            int id = extras.getInt("id");
            MovieDetailTask movieDetailTask = new MovieDetailTask(this);
            movieDetailTask.setMovieDetailLoader(this);
            movieDetailTask.execute("https://tiagoaguiar.co/api/netflix/" + id);
        }


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onResult(MovieDetail movieDetail) {
        txtTitle.setText(movieDetail.getMovie().getTitle());
        txtDesc.setText(movieDetail.getMovie().getDescription());
        txtCast.setText(movieDetail.getMovie().getCast());
        movieAdapter.setMovies(movieDetail.getMoviesSimilar());
        ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask(imgMovie);
        imageDownloaderTask.setShadowEnable(true);
        imageDownloaderTask.execute(movieDetail.getMovie().getCoverUrl());
        movieAdapter.notifyDataSetChanged();
    }

    private static class MovieHolder extends RecyclerView.ViewHolder {

        private final ImageView img_cover;

        public MovieHolder(@NonNull View itemView) {
            super(itemView);
            img_cover = itemView.findViewById(R.id.img_cover_test);
        }
    }

    private class MovieAdapter extends RecyclerView.Adapter<MovieHolder> {

        private List<Movie> movies;

        private MovieAdapter(List<Movie> movies) {
            this.movies = movies;
        }

        public void setMovies(List<Movie> movies) {
            this.movies.clear();
            this.movies.addAll(movies);
        }

        @NonNull
        @Override
        public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MovieHolder(getLayoutInflater().inflate(R.layout.movie_item_similar, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
            Movie movie = movies.get(position);
            new ImageDownloaderTask(holder.img_cover).execute(movie.getCoverUrl());
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }
    }

}